package core;

public class MainCalc {
    int intValue = 0;


    public MainCalc(int intValue){
        this.intValue = intValue;
    }

    public int getIntValue(){
        return intValue;
    }

    public void setIntValue(int intValue){
        this.intValue = intValue;
    }
}
