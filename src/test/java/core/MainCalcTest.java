package core;

import org.junit.Assert;
import org.junit.Test;

public class MainCalcTest {

    @Test
    public void simpleTest(){
        MainCalc calc = new MainCalc(3);
        Assert.assertEquals(3, calc.getIntValue());
        calc.setIntValue(10);
        Assert.assertEquals(10, calc.getIntValue());
    }
}
